;;; Installation of resources
;;;  1. Assumes that emacs-utils are already installed and emacs-utils is
;;;     in the path-string (0th argument).

;;;  2. Assumes project directory (1st arg) has a source directory src
;;;     already created.


;;; path-string ps is a : separated sequence of directories or nil
(setq ps (or (elt argv 0) ""))
(message ps)
;;; (setq ps
;;; "~/top/pub/orgmode/publishing-tutorial:~/tmp/r2/publisher:~/tmp/r2/emacs-utils")

(setq load-path
	  (append (split-string ps ":")
			  load-path))

;;; ps is prepended to the load-path
;;; load-path is prepended with ps
(require 'emacs-funs)

;;; project directory
(setq *prj-dir* (as-dir (elt argv 1)))
;;; (setq *prj-dir* "~/top/pub/orgmode/publishing-tutorial")


(setq load-path (cons *prj-dir* load-path))
(require 'prj-config)

;;; this gives us the variables
;;; exporter
;;; emacs
;;; theme

;;; directory external to prj where reusuable
;;; components get installed
(setq *ext-dir* (as-dir (elt argv 2)))



;;; repos
(defvar catalog-repo "https://gitlab.com/vxcg/pub/orgmode/org-config-catalog.git")

;;; *ext-dir* dependent directory definitions:
(setq catalog-name "catalog")
(setq *catalog-dir*       (concat-as-dir *ext-dir* catalog-name))
(setq *exporters-dir*     (concat-as-dir *ext-dir* "exporters"))
(setq *org-distros-dir*   (concat-as-dir *ext-dir* "org-distros"))
(setq *publishers-dir*     (concat-as-dir *ext-dir* "publishers"))
(setq *themes-dir*         (concat-as-dir *ext-dir* "themes"))
(setq *utils-dir*         (concat-as-dir *ext-dir* "utils"))

;;; From repositories
(setq *bash-utils-dir*    (concat-as-dir *utils-dir* "bash-utils"))
(setq *emacs-utils-dir*   (concat-as-dir *utils-dir* "emacs-utils"))

;;; From project
(setq *build-dir*         (concat-as-dir *prj-dir* "build"))
(setq *src-dir*           (concat-as-dir *prj-dir* "src"))
(setq *exp-name*          "exp")
(setq *exp-dir*           (concat-as-dir *prj-dir* *exp-name*))
;;; exporter-dir 
(setq exporter-dir (concat-as-dir *exporters-dir* (symbol-name exporter)))


(defun mkdirs-reusable ()
  (cmd "mkdir -p %s %s %s %s %s"
						 *exporters-dir*
						 *org-distros-dir*
						 *publishers-dir*
						 *themes-dir*
						 *utils-dir*))


(defun mkdirs-prj ()
  (cmd "mkdir -p %s" *build-dir*))

(defun install-catalog ()
  (git-install *ext-dir* catalog-repo catalog-name)
  (setq load-path (cons *catalog-dir* load-path))
  (require 'catalog))


(defun install-org-distros ()
  (generic-install  *org-distros-dir*  catalog-org-distros))

(defun install-exporter ()
  (let* ((exporter-spec (alist-get exporter catalog-exporters))

		 (repo (alist-get 'repo exporter-spec))
		 (org-distro (alist-get 'org-distro exporter-spec))
		 (org-distro-dir
		  (concat-as-dir *org-distros-dir*
						 (symbol-name (car org-distro)))))
	(git-install *exporters-dir* repo)
	(symlink exporter-dir org-distro-dir "org-distro")))

(defun install-themes ()
  (generic-install *themes-dir* catalog-themes))
	
(defun install-repos ()
  (mkdirs-reusable)
  (mkdirs-prj)
  (install-catalog)
  (install-org-distros)
  (install-themes)
  (install-exporter))
	

(defun mk-links ()
;;; cd prj; ln -s  <exporter-dir> exp
;;; cd src; ln -s  <themes-dir>  
  (cmd "cd %s; ln -sfT %s exp" *prj-dir* exporter-dir)
  (cmd "cd %s; ln -sf %s"      *src-dir* *themes-dir*))




;;; src:
;;; #+include: org-templates/level-0.org
;;; #+include: 

(defun wf ()
  (install-repos)
  (mk-links))
	
(wf)
