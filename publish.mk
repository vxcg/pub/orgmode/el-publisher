# Usage:
# ------
# 1. copy this file to the client's location
# 2. 
SHELL:=/bin/bash
home=$(shell echo $$HOME)
# PARAMETERS to make with defaults
prj-dir=$(shell pwd -P)
infra-dir=~/tmp/infra
exporter=org-v9
emacs=emacs
force=nil

# Infra resources
exporter-dir=${infra-dir}/exporters/${exporter}
emacs-utils-dir=${infra-dir}/utils/emacs-utils


# Local Directories
# ------------------
build-dir=${prj-dir}/build
build-docs-dir=${build-dir}/docs
build-code-dir=${build-dir}/code
src-dir=${prj-dir}/src
exp-link-name=exp
exp-dir=${prj-dir}/${exp-link-name}

all: init

init: mk-dirs mk-exp-link mk-themes-link

mk-dirs:
	(mkdir -p ${src-dir} ${build-docs-dir} ${build-code-dir})

mk-exp-link:
	(cd ${prj-dir}; ln -sf ${infra-dir}/exporters/${exporter} ${exp-link-name})

mk-themes-link:
	(cd ${src-dir}; ln -sf ${infra-dir}/themes)

themes: init
	(rsync -a ${src-dir}/themes/ ${build-docs-dir}/themes)

publish: themes pub

pub: 
	(${emacs} -q --script ${exporter-dir}/elisp/publish-cli-proxy2.el \
	"path=${emacs-utils-dir}:${exporter-dir}/elisp" \
	"prj-dir=${prj-dir}" "exporter-dir=${exporter-dir}" "force=${force}")











