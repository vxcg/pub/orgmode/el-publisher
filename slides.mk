# printing reveal slides using decktape
# -------------------------------------
# assumes (node and) decktape are installed
# -----------------------------------------
# CUSTOMIZABLE
decktape=~/apps/node_modules/.bin/decktape

# directory relative to src-dir
# CUSTOMIZABLE
slides=slides

# name (sans extension) of slides file
# CUSTOMIZABLE
slides-file=slides


src-slides-dir=${src-dir}/${slides}
docs-slides-dir=${build-docs-dir}/${slides}
code-slides-dir=${build-code-dir}/${slides}

slides-html=${docs-slides-dir}/${slides-file}.html
slides-pdf=${docs-slides-dir}/${slides-file}.pdf

cli=publish-cli-proxy2

# the next several target have to do with (org-reveal) slides

slides-init:
	(mkdir -p ${docs-slides-dir}; mkdir -p ${code-slides-dir})

slides-plugin:
	(mkdir -p ${src-slides-dir}/plugin; cd ${src-slides-dir}/plugin; \
    ln -sf ../../themes/org-reveal/Reveal.js-TOC-Progress/plugin/toc-progress)


slides: slides-init slides-plugin
	(${emacs} -q --script ${exp-dir}/elisp/${cli}.el \
	"path=${emacs-utils-dir}:${exp-dir}/elisp" "prj-dir=${prj-dir}" \
	"src-dir=${src-slides-dir}" "docs-dir=${docs-slides-dir}" \
	"code-dir=${code-slides-dir}"  "force=${force}" "slides=t")

# assumes decktape is installed
slides-pdf:
	@echo "conv to pdf using decktape"
	${decktape} -s 1280x760 reveal ${slides-html} ${slides-pdf}

clean-slides:
	(\rm -rf ${src-slides-dir}/plugin)

