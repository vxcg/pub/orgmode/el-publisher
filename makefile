SHELL:=/bin/bash
home=$(shell echo $$HOME)
prj-dir=$(shell pwd -P)
# Custom parameters
# =================
# org-infra-dir: directory for reusble resources:
# CUSTOMIZABLE
org-infra-dir=../..
#emacs=emacs-25.3
emacs=emacs
exp=exp
exp-dir=${prj-dir}/${exp}
# Common Directories
# -------------------
build-dir=${prj-dir}/build
utils-dir=${org-infra-dir}/utils

# Repositories
# ------------
emacs-utils-repo=https://gitlab.com/vxcg/pub/emacs/emacs-utils.git
emacs-utils-dir=${utils-dir}/emacs-utils

init:
	(mkdir -p ${org-infra-dir} ${utils-dir})

install-utils: init
	(source ./init.sh; git-install ${emacs-utils-dir} ${emacs-utils-repo})

install-resources: 
	(${emacs} -q --script ./install-resources.el \
	${emacs-utils-dir} ${prj-dir} ${org-infra-dir})
